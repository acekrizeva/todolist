import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to To Do List");
        System.out.println("Press\n" +
                "0 - To add task\n" +
                "1 - To edit task\n" +
                "2 - To view tasks" +
                "Q to quit\n");
        String choice = scanner.nextLine();
        switch (choice) {
            case "0":
                addTask = addTask();
                break;
            case "1":
                editTask = editTask();
                break;
            case "2":
                viewTask = viewTask();

            default:
                return;
        }
        initialize();
    }

    public static void initialize() {

        final String dbURL = "jdbc:mysql://localhost:3306/todolist";
        final String user = "root";
        final String password = "Salat3t1s32dll";

        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {

            boolean quit = false;
            int choice;

            Database.printInstructions();

            while (!quit) {
                System.out.println("Enter your choice: ");
                choice = scanner.nextInt();
                scanner.nextLine();

                switch (choice) {
                    case 0:
                        //add task
                        Database.addTask();
                        break;
                    case 1:
                        //edit task
                        Database.editTask();
                        break;
                    case 2:
                        //view task
                        Database.viewTask();
                        break;
                    case 10:
                        quit = true;
                        break;
                }
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong");
        }
    }
    }

