import java.util.Date;

public class Task {

        public int taskNr;
        public String taskDescription;
        public String taskStatus;
        public Date taskDate;

        public Task(int taskNr, String taskDescription, String taskStatus, Date taskDate) {
            this.taskNr = taskNr;
            this.taskDescription = taskDescription;
            this.taskStatus = taskStatus;
            this.taskDate = taskDate;
        }
    }
