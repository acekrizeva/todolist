import java.sql.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Database {
    static final String dbURL = "jdbc:mysql://localhost:3306/tradingjournal";
    static final String user = "root";
    static final String password = "Salat3t1s32dll";

    static Scanner scanner = new Scanner(System.in);

    public static void printInstructions() {
        System.out.println("Press\n" +
                "0 - To add task\n" +
                "1 - To edit task\n" +
                "2 - To view tasks" +
                "Q to quit\n");    }

    public static void viewTask(Connection conn) throws SQLException {
        String sql = "SELECT * FROM tasks;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            int taskNr = resultSet.getInt(1);
            String taskDescription = resultSet.getString(2);
            String taskStatus = resultSet.getString(3);
            String taskDate = resultSet.getString(4);

            String output = "Task info: \n\t Nr.: %d \n\t Task Description: %s \n\t " +
                    "Task Statuss: %s \n\t Date: %s";

            System.out.printf((output) + "%n", taskNr, taskDescription, taskStatus, taskDate);
        }
    }

    public static void addTask(Connection conn) throws SQLException {

        String sql = "INSERT INTO tasks (Task_Nr,Task_Description,Task_Statuss, Task_date) VALUES (?,?,?,?);";

        System.out.println("Enter Task Nr: ");
        String taskNr = scanner.nextLine();

        System.out.println("Enter Task Description: ");
        String taskDescription = scanner.nextLine();

        System.out.println("Enter Task Status: ");
        String taskStatus = scanner.nextLine();

        System.out.println("Enter Task date (YYYY-MM-DD): ");
        String taskDate = scanner.nextLine();

        PreparedStatement preparedStatement = conn.prepareStatement(sql);

        preparedStatement.setString(1, taskNr);
        preparedStatement.setString(2, taskDescription);
        preparedStatement.setString(3, taskStatus);
        preparedStatement.setString(4, taskDate);

        int rowInserted = preparedStatement.executeUpdate();

        if (rowInserted > 0){
            System.out.println("A new task was inserted successfully");
        }else{
            System.out.println("Something went wrong");
        }
    }

        }




